svnMonitor
===
简介
---
服务器端对svn日志进行监控，有变更时通过webSocket推送到订阅它的客户端(chrome插件)，在桌面弹出通知。效果如下：<br />
![preview01](https://git.oschina.net/zuojianghua/svnMonitor/raw/master/public/chrome/preview20130808170214.jpg "preview01")

使用帮助
---
### 服务器端
SVN地址、用户名、密码配置在bin/monitor.php中<br />
WS地址及端口号配置在bin/push-server.php中<br />
PHP需要安装ZeroMQ的扩展<br />
默认使用的ZeroMQ端口号为5555<br />
默认使用的webSocket端口号为8098<br />

监控器运行请执行<br />
    php bin/monitor.php<br />
消息推送运行请执行<br />
    php bin/push-server.php<br />

### 客户端
需要chrome浏览器(版本18以上)，或者使用chrome内核的浏览器(例如360极速，枫树等等)<br />
打开浏览器扩展程序的开发者模式<br />
安装/public/chrome/svnMonitor.crx<br />
在扩展程序选项中将WS地址设为您的服务器地址，需要加端口号<br />

一些参考
---
1. ratchet [http://socketo.me/](http://socketo.me/)<br />
2. ZeroMQ [http://zeromq.org/](http://zeromq.org/)<br />
3. React/ZMQ [http://reactphp.org/](http://reactphp.org/)<br />
4. composer [http://getcomposer.org/download/](http://getcomposer.org/download/)<br />
5. chrome插件开发文档 [https://developer.chrome.com/extensions/index.html](https://developer.chrome.com/extensions/index.html) 被墙可以使用360的[http://open.se.360.cn/open/extension_dev/overview.html](http://open.se.360.cn/open/extension_dev/overview.html)代替<br />
6. windows下的ZeroMQ PHP扩展，官网下载链接已经打不开了，可以使用[http://178.79.157.189/~mikko/win32/php-zmq-win32.zip](http://178.79.157.189/~mikko/win32/php-zmq-win32.zip)代替<br />