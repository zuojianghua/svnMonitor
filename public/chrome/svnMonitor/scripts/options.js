$(document).ready(function(){
    options.isActivated.checked = JSON.parse(localStorage.isActivated);
    options.frequency.value = localStorage.frequency;
    options.server.value = localStorage.server;

    options.isActivated.onchange = function() {
        localStorage.isActivated = options.isActivated.checked;
    };

    options.frequency.onchange = function() {
        localStorage.frequency = options.frequency.value;
    };

    options.server.onchange = function() {
        localStorage.server = options.server.value;
    };
});