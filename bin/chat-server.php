<?php

use Ratchet\Server\IoServer;
use app\Chat;

require dirname(__DIR__) . '/vendor/autoload.php';

$server = IoServer::factory(
                new Chat()
                , 8089
);

$server->run();