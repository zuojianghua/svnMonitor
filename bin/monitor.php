<?php

ob_implicit_flush(true);
session_start();
/**
 * 介绍
 * SVN代码提交检测，每分钟检测一次，计算出有效代码行，并通过消息队列发送到订阅的客户端
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * @author zuojianghua<zuojianghua@gmail.com>
 * @date 2013-08-06
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * 需要安装ZeroMQ以及ZeroMQ的PHP扩展
 * ZeroMQ端口号 5555
 * Websocket端口号 8098
 *
 * 监控器运行请执行 php bin/monitor.php
 * 消息推送运行请执行 php bin/push-server.php
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */

### 环境设置 ##################################################################
set_time_limit(0);
error_reporting(0);
ini_set('display_errors', 'Off');
header("content-type:text/html;charset=utf-8");
putenv('LANG=en_US.UTF-8');

### 参数设置 ##################################################################

//$planform = 'windows';                                                     // windows or linux
$svnname = 'zuojh';                                                          // SVN帐号
$svnpass = 'yourpassword';                                                   // SVN密码
$svnurl = 'https://path/to/';                              // 代码仓库目录
$svncmd = 'C:\zuojianghua@gmail.com\program\Subversion\bin\svn.exe';         // SVN命令路径
//$svncmd = 'sudo /usr/bin/svn';
$reg = '`\.php|\.html|\.htm|\.xml|\.css|\.js|\.txt$`i';                      // 过滤检查的文件
//$sudoconfig = '/data/www/fx7/works.conf';
### 主程序 ####################################################################

$context = new ZMQContext();
$socket = $context->getSocket(ZMQ::SOCKET_PUSH, 'my pusher');
$socket->connect("tcp://localhost:5555");

do {
    //获取当前版本号
    $info = svninfo();
    if (0 == $info['status']) {
        $obj = simplexml_load_string($info['result']);
        $version = $obj->entry->attributes();
        $version = (int) $version['revision'][0];
    }
    $aryLog = array();
    $last_version = $_SESSION['version'];
    if ($last_version) {
        //与session不为空则与session中的版本号进行比较
        if ($version > (int) $last_version) {
            //如果有更新则计算更新结果
            //echo 'SVN有更新';
            //刷新svn日志，找到session中版本号至今的所有的版本记录
            $log = svnlog($last_version, $version);
            $obj = simplexml_load_string($log['result']);
            //print_r($obj);
            //比对每个版本，统计出文件变更及代码行数（可以缓存统计结果）
            $aryLog = json_decode(json_encode($obj), TRUE);
            //print_r($aryLog);
            foreach ($aryLog['logentry'] as $key => $chgVersion) {
                //如果是上版本的更新信息则去掉
                if ($last_version == $chgVersion['@attributes']['revision']) {
                    unset($aryLog['logentry'][$key]);
                } else {
                    if (!is_array($chgVersion['paths']['path'])) {
                        $aryLog['logentry'][$key]['paths']['path'] = array($aryLog['logentry'][$key]['paths']['path']);
                    }
                    foreach ($aryLog['logentry'][$key]['paths']['path'] as $key2 => $path) {
                        $diff = svndiff($chgVersion['@attributes']['revision'], $path);
                        if ($diff['status'] == 0) {
                            //查找修订日志成功
                            $count = analyzeSvnDiff($diff['result']);
                        } else {
                            //查找失败。可能是新增的文件
                            if (false !== strstr($diff['result'][0], '版本库位置不在版本') && preg_match($reg, $path)) {
                                $count = analyzeSvnNew($chgVersion['@attributes']['revision'], $path);
                            } else {
                                $count = 0;
                            }
                        }
                        //将代码行数信息以版本号为键名压入信息数组
                        $aryLog['logentry'][$key]['paths']['count'][$key2] = $count;
                    }
                }
            }
        }
    }
    //推送信息到客户端，如果有信息的话
    if (!empty($aryLog)) {
        $socket->send(json_encode($aryLog));
    }
    //将当前版本号记录到session
    $_SESSION['version'] = $version;
    //每隔60秒检查一次
    echo 'check svn ' . $_SESSION['version'] . ' at ' . date('Y-m-d H:i:s') . "\r\n";
    ob_flush();
    sleep(60);
} while (true);

### 方法库 ####################################################################
/**
 * 以XML格式获取SVN信息
 * @author zuojianghua<zuojianghua@gmail.com>
 * @date 2013-08-06
 * @return array 返回由执行状态status和执行结果result组成的数组
 */

function svninfo() {
    global $svnurl;
    global $svnname;
    global $svnpass;
    global $svncmd;

    $command = $svncmd . " info $svnurl --xml --username $svnname --password $svnpass --non-interactive 2>&1";

    $output = '';
    $status = 0;
    exec($command, $output, $status);
    $output = implode("\n", $output) . "\n";
    return array('status' => $status, 'result' => $output);
}

/**
 * 以XML格式获取SVN修改日志
 * @author zuojianghua<zuojianghua@gmail.com>
 * @date 2013-02-27
 * @return array 返回由执行状态status和执行结果result组成的数组
 */
function svnlog($start, $end) {
    global $svnurl;
    global $svnname;
    global $svnpass;
    global $svncmd;

    $command = $svncmd . " log $svnurl -r " . $end . ":" . $start . " -v --xml --username $svnname --password $svnpass --non-interactive 2>&1";
    //$command = "sudo /usr/bin/svn log $svnurl -l 100 -v --xml --username $svnname --password $svnpass --non-interactive 2>&1 <$sudoconfig";
    //$command = "sudo /usr/bin/svn log $svnurl -l 100 -v --xml --username $svnname --password $svnpass --non-interactive /data/www/fx7/works.log <$sudoconfig";
    $output = '';
    $status = 0;
    exec($command, $output, $status);
    $output = implode("\n", $output) . "\n";
    return array('status' => $status, 'result' => $output);
}

/**
 * 获取某版本某文件的修订日志
 * @author zuojianghua<zuojianghua@gmail.com>
 * @date 2013-02-28
 * @param string $version 当前版本号
 * @param string $url 修订的文件路径
 * @return array 返回由执行状态status和执行结果result组成的数组
 */
function svndiff($version, $url) {
    global $svnurl;
    global $svnname;
    global $svnpass;
    global $svncmd;

    $last = $version - 1;
    $command = $svncmd . " diff -r " . $last . ":$version " . $svnurl . $url . " --username $svnname --password $svnpass 2>&1";
    $output = '';
    $status = 0;
    exec($command, $output, $status);
    //$output = implode("\n", $output) . "\n";
    return array('status' => $status, 'result' => $output);
}

/**
 * 获取某文件本次修订的代码工作量
 * @author zuojianghua<zuojianghua@gmail.com>
 * @date 2013-02-28
 * @param array $ary_log SVN修订记录数组
 * @return int 返回代码工作量行数
 */
function analyzeSvnDiff($ary_log) {
    $count = 0;
    foreach ($ary_log as $line => $str) {
        if ($str[0] == '+' && $str[1] != '+') {
            $count++;
        }
        if ($str[0] == '-' && $str[1] != '-' && $ary_log[$line + 1] != '+') {
            $count++;
        }
    }
    return $count;
}

/**
 * 统计SVN中新增文件的代码行数
 * @author zuojianghua<zuojianghua@gmail.com>
 * @date 2013-03-02
 * @param string $svnurl SVN地址
 * @param string $url SVN文件路径
 * @return int 返回新增文件的代码行数
 */
function analyzeSvnNew($version, $url) {
    global $svnurl;
    global $svnname;
    global $svnpass;
    global $svncmd;

    $tmpName = dirname(__FILE__) . '/tmp/' . md5($svnurl . $url . $version) . '.svnfile';
    if (!file_exists($tmpName)) {
        $command = $svncmd . " export -r " . $version . " " . $svnurl . $url . " " . $tmpName . " --username $svnname --password $svnpass 2>&1";
        exec($command);
    }

    $content = file_get_contents($tmpName);
    $contentAry = explode("\n", $content);
    return count($contentAry);
}

##############################################################################