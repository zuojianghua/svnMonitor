@ECHO Off &CLS & mode con lines=20 cols=50 & title 启动监控和推送服务
PATH PATH C:\WINDOWS;C:\WINDOWS\COMMAND;C:\WINDOWS\system32;c:\wamp\bin\php\php5.3.10;
set CURRENT_DIR=%cd%
CHOICE /C YN /M 按Y启动监控,按N取消监控.
if errorlevel 2 goto end
if errorlevel 1 goto start
:start
start cmd /c "%CURRENT_DIR%\push-server.bat"
start cmd /c "%CURRENT_DIR%\monitor.bat"
echo 服务已经启动,按任意键退出启动器
goto end
:end
rem pause
echo 拜拜.
exit